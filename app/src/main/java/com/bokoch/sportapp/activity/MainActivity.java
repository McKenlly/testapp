package com.bokoch.sportapp.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.bokoch.sportapp.Model.CategoryEnum;
import com.bokoch.sportapp.R;
import com.bokoch.sportapp.fragment.ArticlePreviewFragment;
import com.bokoch.sportapp.fragment.CategoryFragment;

import java.util.Objects;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(MainActivity.class.getSimpleName(), "Article");
        setContentView(R.layout.activity_main);
        loadFragment(CategoryFragment.newInstance(CategoryEnum.hockey.name()));
        BottomNavigationView navigation = findViewById(R.id.tabs_category);
        navigation.setOnNavigationItemSelectedListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.action_hockey:
                fragment = CategoryFragment.newInstance(CategoryEnum.hockey.name());
                break;

            case R.id.action_tennis:
                fragment = CategoryFragment.newInstance(CategoryEnum.tennis.name());
                break;
            case R.id.action_basketball:
                fragment = CategoryFragment.newInstance(CategoryEnum.basketball.name());
                break;
            case R.id.action_football:
                fragment = CategoryFragment.newInstance(CategoryEnum.football.name());
                break;

            case R.id.action_volleyball:
                fragment = CategoryFragment.newInstance(CategoryEnum.volleyball.name());
                break;
        }
        return loadFragment(fragment);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
