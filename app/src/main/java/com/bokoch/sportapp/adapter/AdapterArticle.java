package com.bokoch.sportapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bokoch.sportapp.Model.MicroEvent;
import com.bokoch.sportapp.R;

import java.util.List;


public class AdapterArticle extends RecyclerView.Adapter<AdapterArticle.ViewHolder> {

    private List<MicroEvent> mMicroEvents;

    public AdapterArticle(List<MicroEvent> mMicroEvents) {
        this.mMicroEvents = mMicroEvents;
    }

    public void notifyDataSetChanged(List<MicroEvent> events) {
        if (events!= null) {
            mMicroEvents = events;
            notifyDataSetChanged();
        }
    }
    @Override
    public AdapterArticle.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_micro_event, parent, false);
        return new AdapterArticle.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterArticle.ViewHolder holder, int position) {
        MicroEvent microEvent = mMicroEvents.get(position);
            holder.mHeader.setText(microEvent.getHeader());
            holder.mText.setText(microEvent.getText());
    }

    @Override
    public int getItemCount() {
        if (mMicroEvents == null)
            return 0;
        return mMicroEvents.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mHeader;
        TextView mText;

        public ViewHolder(View itemView) {
            super(itemView);
            mText = (TextView) itemView.findViewById(R.id.text_micro_event);
        }
    }
}
