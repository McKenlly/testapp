package com.bokoch.sportapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bokoch.sportapp.Model.Event;
import com.bokoch.sportapp.R;
import com.bokoch.sportapp.fragment.ArticlePreviewFragment;

import java.util.List;

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolder> {

    private Fragment mFragment;

    private List<Event> mEvents;

    public AdapterRecyclerView(List<Event> mEvents, Fragment fragment) {
        this.mEvents = mEvents;
        this.mFragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_arcticle, parent, false);
        return new ViewHolder(v);
    }

    public void notifyDataSetChanged(List<Event> events) {
        if (events!= null) {
            mEvents = events;
            notifyDataSetChanged();
        }
    }


    private Fragment getFragment() {
        return mFragment;
    }
    public List<Event> getData() {
        return mEvents;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Event event = mEvents.get(position);
        holder.mTitle.setText(event.getTitle());
        holder.mPreview.setText(event.getPreview());
        holder.mCoefficient.setText(event.getCoefficient());
        holder.mPlace.setText(event.getPlace());
        holder.mPreview.setText(event.getPreview());
        holder.mTime.setText(event.getTime());

    }

    @Override
    public int getItemCount() {
        if (mEvents == null)
            return 0;
        return mEvents.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mCoefficient;
        TextView mPreview;
        TextView mPlace;
        TextView mTime;
        CardView mCardView;
        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.title);
            mPlace = itemView.findViewById(R.id.place);
            mPreview = itemView.findViewById(R.id.preview);
            mCoefficient = itemView.findViewById(R.id.coefficient);
            mTime = itemView.findViewById(R.id.time);
            mCardView = itemView.findViewById(R.id.item_card_layout);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openArticle(getAdapterPosition());
                }
            });
        }
        private void openArticle(int pos) {
            ArticlePreviewFragment articleFragment = ArticlePreviewFragment.newInstance(getData().get(pos).getArticleRequest());

            getFragment()
                    .getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, articleFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }
}
