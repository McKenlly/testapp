package com.bokoch.sportapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bokoch.sportapp.Model.MicroEvent;
import com.bokoch.sportapp.R;
import com.bokoch.sportapp.Util.UtilService;

import java.util.List;

public class MicroEventRecyclerView extends RecyclerView.Adapter<MicroEventRecyclerView.ViewHolder> {

    private List<MicroEvent> mEvents;

    public MicroEventRecyclerView(List<MicroEvent> mEvents) {
        this.mEvents = mEvents;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_micro_event, parent, false);
        return new ViewHolder(v);
    }


    public List<MicroEvent> getData() {
        return mEvents;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MicroEvent microEvent = mEvents.get(position);
        UtilService.checkEmpty(microEvent.getText(), holder.mText);
        UtilService.checkEmpty(microEvent.getHeader(), holder.mHeader);
    }

    @Override
    public int getItemCount() {
        if (mEvents == null)
            return 0;
        return mEvents.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mText;
        private TextView mHeader;
        public ViewHolder(View itemView) {
            super(itemView);
            mText = itemView.findViewById(R.id.text_micro_event);
            mHeader = itemView.findViewById(R.id.header_micro_event);
        }

    }
}

