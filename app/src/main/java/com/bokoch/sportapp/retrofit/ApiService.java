package com.bokoch.sportapp.retrofit;

import android.app.Application;

import com.bokoch.sportapp.retrofit.MikonatururiApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService  extends Application {

    private static MikonatururiApi sMikonatururiApi;
    private Retrofit mRetrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://mikonatoruri.win") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        sMikonatururiApi = mRetrofit.create(MikonatururiApi.class); //Создаем объект, при помощи которого будем выполнять запросы
    }

    public static MikonatururiApi getApi() {
        return sMikonatururiApi;
    }
}
