package com.bokoch.sportapp.retrofit;

import com.bokoch.sportapp.Model.ArrayEvent;
import com.bokoch.sportapp.Model.Article;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MikonatururiApi {
    @GET("post.php")
    Call<Article> getArticle(@Query("article") String name);

    @GET("list.php")
    Call<ArrayEvent> getEvents(@Query("category") String name);
}
