package com.bokoch.sportapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class MicroEvent implements Serializable {
    @SerializedName("header") @Expose @Getter @Setter private String header;
    @SerializedName("text") @Expose @Getter @Setter private String text;
}
