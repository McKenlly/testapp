package com.bokoch.sportapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Article implements Serializable {
    @SerializedName("team1") @Expose @Getter @Setter private String team1;
    @SerializedName("team2") @Expose @Getter @Setter private String team2;
    @SerializedName("time")  @Expose @Getter @Setter private String time;
    @SerializedName("tournament") @Expose @Getter @Setter private String tournament;
    @SerializedName("place") @Expose @Getter @Setter private String place;
    @SerializedName("article")@Expose  @Getter @Setter private List<MicroEvent> microEvents;
    @SerializedName("prediction")@Expose  @Getter @Setter private String prediction;
}
