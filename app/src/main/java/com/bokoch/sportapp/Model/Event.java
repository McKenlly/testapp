package com.bokoch.sportapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class Event implements Serializable {
    @SerializedName("title") @Expose @Getter @Setter private String title;
    @SerializedName("coefficient") @Expose  @Getter @Setter private String coefficient;
    @SerializedName("time") @Expose @Getter @Setter private String time;
    @SerializedName("place") @Expose  @Getter @Setter private String place;
    @SerializedName("preview") @Expose  @Getter @Setter private String preview;
    @SerializedName("article") @Expose  @Getter @Setter private String articleRequest;

}
