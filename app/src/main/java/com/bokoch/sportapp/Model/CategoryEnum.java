package com.bokoch.sportapp.Model;

public enum CategoryEnum {
    basketball,
    tennis,
    football,
    hockey,
    volleyball
}
