package com.bokoch.sportapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ArrayEvent implements Serializable {
    @SerializedName("events") @Expose
    @Getter
    @Setter
    private List<Event> mEvents = new ArrayList<>();
}
