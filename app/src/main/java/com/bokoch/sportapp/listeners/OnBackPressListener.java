package com.bokoch.sportapp.listeners;

public interface OnBackPressListener {

    public boolean onBackPressed();
}
