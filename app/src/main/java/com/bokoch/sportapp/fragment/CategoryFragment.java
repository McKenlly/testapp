package com.bokoch.sportapp.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bokoch.sportapp.adapter.AdapterRecyclerView;
import com.bokoch.sportapp.Model.ArrayEvent;
import com.bokoch.sportapp.Model.Event;
import com.bokoch.sportapp.R;
import com.bokoch.sportapp.retrofit.ApiService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends Fragment
                                implements SwipeRefreshLayout.OnRefreshListener {

    @SuppressLint("StaticFieldLeak")
    private static CategoryFragment INSTANCE = null;

    private final static String ARG_CATEGORY_NAME = "category";
    private final static String ARG_DATA = "data";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private List<Event> mEvents;
    private String mCategoryName;
    private ProgressBar mProgressBar;
    AdapterRecyclerView mAdapter;

    public static CategoryFragment newInstance(String articleUri) {
        if (INSTANCE == null || !INSTANCE.mCategoryName.equals(articleUri)) {
            Bundle bundle = new Bundle();
            bundle.putString(ARG_CATEGORY_NAME, articleUri);
            INSTANCE = new CategoryFragment();
            INSTANCE.setArguments(bundle);
        }
        return INSTANCE;
    }
    @SuppressLint("ValidFragment")
    private CategoryFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mCategoryName = getArguments().getString(ARG_CATEGORY_NAME);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getActivity().setTitle(getString(R.string.app_name));
        View v = inflater.inflate(R.layout.fragment_category, container, false);
        init(v);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new AdapterRecyclerView(mEvents, this);
        mRecyclerView.setAdapter(mAdapter);
        if (savedInstanceState == null) {
            showLoadingIndicator(true);
            getEvents();
        } else {
            mEvents = (List<Event>)savedInstanceState.getSerializable(ARG_DATA);
            mAdapter.notifyDataSetChanged(mEvents);
        }

        return v;
    }

    private void getEvents() {
        ApiService.getApi().getEvents(mCategoryName).enqueue(new Callback<ArrayEvent>() {
            @Override
            public void onResponse(@NonNull Call<ArrayEvent> call, @NonNull Response<ArrayEvent> response) {
                mEvents = response.body().getMEvents();
                if (mEvents != null) {
                    mAdapter.notifyDataSetChanged(response.body().getMEvents());
                }
                mSwipeRefreshLayout.setRefreshing(false);
                mRecyclerView.getAdapter().notifyDataSetChanged();
                showLoadingIndicator(false);
            }

            @Override
            public void onFailure(Call<ArrayEvent> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                showLoadingIndicator(false);

            }
        });
    }
    private void showLoadingIndicator(boolean show) {
        if (show) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mEvents = new ArrayList<>();
        mRecyclerView.getAdapter().notifyDataSetChanged();
        mAdapter.notifyDataSetChanged(new ArrayList<Event>());
        getEvents();

    }

    public void init(View v) {
        mRecyclerView = v.findViewById(R.id.category_recycler_view);
        mSwipeRefreshLayout = v.findViewById(R.id.refresh_category);
        mProgressBar = v.findViewById(R.id.loading_events_category);
        mEvents = new ArrayList<>();

    }

    /*@Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore state members from saved instance
        mCurrentScore = savedInstanceState.getInt(STATE_SCORE);
        mCurrentLevel = savedInstanceState.getInt(STATE_LEVEL);
    }*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mEvents != null) {
            outState.putSerializable(ARG_DATA, (Serializable) mEvents);
        }
    }
}
