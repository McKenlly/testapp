package com.bokoch.sportapp.fragment;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Lifecycle;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bokoch.sportapp.Model.Article;
import com.bokoch.sportapp.Model.MicroEvent;
import com.bokoch.sportapp.R;
import com.bokoch.sportapp.retrofit.ApiService;


import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bokoch.sportapp.Util.UtilService.checkEmpty;

public class ArticlePreviewFragment extends Fragment implements View.OnClickListener {

    private static final String LOG = ArticlePreviewFragment.class.getName();

    @SuppressLint("StaticFieldLeak")
    private static ArticlePreviewFragment INSTANCE = null;

    private final static String ARG_EVENT_NAME = "event";
    private final static String ARG_ARTICLE = "article";

    private Article mArticle;
    private String mArticleLink;
    private TextView mTime;
    private TextView mTournament;
    private TextView mPrediction;
    private TextView mPlace;
    private Button mButtonNext;
    private ProgressBar mProgressBar;
    private CardView mCardView;

    public static ArticlePreviewFragment newInstance(String eventLink) {
        if (INSTANCE == null || !INSTANCE.mArticleLink.equals(eventLink)) {
            Bundle bundle = new Bundle();
            bundle.putString(ARG_EVENT_NAME, eventLink);
            INSTANCE = new ArticlePreviewFragment();
            INSTANCE.setArguments(bundle);
        }
        return INSTANCE;
    }

    @SuppressLint("ValidFragment")
    private ArticlePreviewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mArticleLink = getArguments().getString(ARG_EVENT_NAME);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main_article, container, false);
        init(v);
        mButtonNext.setOnClickListener(this);
        setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            mArticle = (Article) savedInstanceState.getSerializable(ARG_ARTICLE);
            setupParam();
        } else {
            showLoadingIndicator(true);
            getArticle();
        }
        return v;
    }

    private void getArticle() {
        ApiService.getApi().getArticle(mArticleLink).enqueue(new Callback<Article>() {
            @Override
            public void onResponse(@NonNull Call<Article> call, @NonNull Response<Article> response) {
                mArticle = response.body();
                if (mArticle != null) {
                    setupParam();
                    mCardView.setVisibility(View.VISIBLE);
                }
                showLoadingIndicator(false);

            }

            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                showLoadingIndicator(false);
            }
        });
    }

    public void init(View v) {
        mCardView = v.findViewById(R.id.item_card_layout);
        mTime = v.findViewById(R.id.time_event);
        mPlace = v.findViewById(R.id.place_event);
        mPrediction = v.findViewById(R.id.prediction_event);
        mTournament = v.findViewById(R.id.tournament_event);
        mButtonNext = v.findViewById(R.id.button_events);
        mProgressBar = v.findViewById(R.id.loading_article);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().invalidateOptionsMenu();
    }

    private void setupParam() {
        getActivity().setTitle(mArticle.getTeam1() + ":" + mArticle.getTeam2());
        checkEmpty(mArticle.getTime(), mTime);
        checkEmpty(mArticle.getTournament(), mTournament);
        checkEmpty(mArticle.getPlace(), mPlace);
        checkEmpty(mArticle.getPrediction(), mPrediction);
    }



    private void showLoadingIndicator(boolean show) {
        if (show) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void onClick(View v) {
        if (mArticle != null) {
            List<MicroEvent> microEvents = mArticle.getMicroEvents();
            if (microEvents != null && !microEvents.isEmpty()) {
                openEvents(microEvents);
            } else {
                Toast.makeText(getActivity(), getString(R.string.empty_list_event), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_loading), Toast.LENGTH_SHORT).show();

        }
    }

    private void openEvents(List<MicroEvent> microEvents) {
        MicroEventFragment microEvent = MicroEventFragment.newInstance(microEvents);
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, microEvent)
                .addToBackStack(null)
                .commit();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mArticle != null) {
            outState.putSerializable(ARG_ARTICLE, (Serializable) mArticle);
        }
    }

}