package com.bokoch.sportapp.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bokoch.sportapp.Model.MicroEvent;
import com.bokoch.sportapp.R;
import com.bokoch.sportapp.adapter.MicroEventRecyclerView;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class MicroEventFragment extends Fragment {
    @SuppressLint("StaticFieldLeak")
    private static MicroEventFragment INSTANCE = null;

    private final static String ARG_EVENTS = "events";

//    private ViewPager mViewPager;
    private RecyclerView mRecyclerView;
    private List<MicroEvent> mMicroEvents;

    public static MicroEventFragment newInstance(List<MicroEvent> microEvents) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(ARG_EVENTS, (Serializable) microEvents);
            INSTANCE = new MicroEventFragment();
            INSTANCE.setArguments(bundle);
        return INSTANCE;
    }

    @SuppressLint("ValidFragment")
    private MicroEventFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mMicroEvents = (List<MicroEvent>) getArguments().getSerializable(ARG_EVENTS);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_micro_event, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        mViewPager = v.findViewById(R.id.micro_event_view_pager);
//        mViewPager.setAdapter(new MicroEventViewPagerAdapter(getActivity().getSupportFragmentManager(),
//                                    mMicroEvents));

        mRecyclerView = v.findViewById(R.id.micro_event_recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(new MicroEventRecyclerView(mMicroEvents));
        setHasOptionsMenu(true);
        return v;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().invalidateOptionsMenu();
    }



}
