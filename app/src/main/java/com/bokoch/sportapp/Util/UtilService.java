package com.bokoch.sportapp.Util;

import android.widget.TextView;

import com.bokoch.sportapp.R;

public class UtilService {
    public static void checkEmpty(String name,TextView textView) {
        if (name != null && !name.isEmpty()) {
            textView.setText(name);
        } else {
            textView.setText(R.string.empty_data);
        }
    }
}
